package com.fazi.ffeign.service;

import com.fazi.ffeign.config.FeignClientConfig;
import com.fazi.ffeign.factory.UserServiceFallbackFactory;
import com.fazi.ffeign.service.impl.UserServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @InterfaceName FactoryUserService
 * @Description 工厂方式定义，由于
 * Spring Boot 2.1.0 Spring Cloud Greenwich.M1  版本后，在2个Feign接口类内定义相同的名字，  @FeignClient(name = 相同的名字 就会出现报错，在之前的版本不会提示报错，报错内容为The bean 'XXX.FeignClientSpecification', defined in null, could not be registered. A bean with that name has already been defined in null and overriding is disabled. 」
 * 这是说Feign的注册，有同名的feign的名字重复注册。
 * 所以暂时注掉
 * @Author fazi
 * @Date 2020/3/3 12:30
 * @Version 1.0
 **/
//@FeignClient(name = "f-provide", fallbackFactory = UserServiceFallbackFactory.class)
public interface FactoryUserService {

    @GetMapping(value = "/fazi/user")
    public String getName();
}
