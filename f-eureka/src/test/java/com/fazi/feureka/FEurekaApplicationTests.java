package com.fazi.feureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class FEurekaApplicationTests {

    public static void main(String[] args) {
        SpringApplication.run(FEurekaApplicationTests.class, args);
    }

}
