package com.fazi.fconsumer.command;

import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixObservableCommand;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

/**
 * @ClassName ProvideObservableCommand
 * @Description 用在依赖服务返回多个操作结果的时候
 * 两种执行方式
 * 　　  -observe():返回Obervable对象，他代表了操作的多个结果，他是一个HotObservable
 * 　　  -toObservable():同样返回Observable对象，也代表了操作多个结果，但它返回的是一个Cold Observable。
 * @Author fazi
 * @Date 2020/3/3 11:33
 * @Version 1.0
 **/
public class ProvideObservableCommand extends HystrixObservableCommand<String> {

    protected ProvideObservableCommand() {
        super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("test")));
    }

    // 实现具体的业务处理逻辑
    @Override
    protected Observable<String> construct() {
        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                try {
                    if(!subscriber.isUnsubscribed()) {
                        subscriber.onNext("Hello");
                        int i = 1 / 0; //模拟异常
                        subscriber.onNext("模拟" + "!");
                        subscriber.onCompleted();
                    }
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io());
    }

    @Override
    protected Observable<String> resumeWithFallback() {
        // 实现服务降级处理逻辑
        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                try {
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onNext("失败了！");
                        subscriber.onNext("找大神来排查一下吧！");
                        subscriber.onCompleted();
                    }
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io());
    }


}
