package com.fazi.ffeign.controller;

import com.fazi.ffeign.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName UserController
 * @Description TODO
 * @Author fazi
 * @Date 2020/2/27 22:41
 * @Version 1.0
 **/
@RestController
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping(value = "/fazi/feign/user")
    public String getName(){
        return userService.getName();
    };
}
