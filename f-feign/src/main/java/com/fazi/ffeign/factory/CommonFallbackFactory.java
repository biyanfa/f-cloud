package com.fazi.ffeign.factory;

import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @ClassName CommonFallbackFactory
 * @Description 全局配置降级,，业务层使用的时候 fallbackFactory 属性引用本类
 * @Author fazi
 * @Date 2020/3/3 12:36
 * @Version 1.0
 **/
@Component
public class CommonFallbackFactory implements FallbackFactory {

    @Override
    public Object create(Throwable throwable) {
        throwable.printStackTrace();
        throw new RuntimeException("service not available");
    }
}
