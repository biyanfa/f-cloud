package com.fazi.ffeign.config;

import feign.Logger;
import feign.auth.BasicAuthRequestInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName FeignClientConfig
 * @Description
 * @Author fazi
 * @Date 2020/3/1 23:55
 * @Version 1.0
 **/
@Configuration
public class FeignClientConfig {

    @Value("${fSecurity.name}")
    String name;

    @Value("${fSecurity.password}")
    String password;
    @Bean
    public Logger.Level feignLoggerLevel(){
        // log 的详细程度
        return Logger.Level.FULL;
    }

    public BasicAuthRequestInterceptor getBasicAuthRequestInterceptor(){
        return new BasicAuthRequestInterceptor(name, password);
    }
}
