package com.fazi.ffeign.service;

import com.fazi.ffeign.config.FeignClientConfig;
import com.fazi.ffeign.service.impl.UserServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @InterfaceName UserService
 * @Description 现在所有的服务要通过 zuul 的代理来进行操作对于代理的配置如果要通过 feign 进行访问，
 * 那么在编写 feign 的时候就必须设置代理的服务名称
 * @Author fazi
 * @Date 2020/2/27 22:34
 * @Version 1.0
 **/
@FeignClient(name = "f-provide", configuration = FeignClientConfig.class, fallback = UserServiceImpl.class)
public interface UserService {

    @GetMapping(value = "/fazi/user")
    public String getName();
}
