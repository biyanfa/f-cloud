package com.fazi.ffeign.service.impl;

import com.fazi.ffeign.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @ClassName UserServiceImpl
 * @Description TODO
 * @Author fazi
 * @Date 2020/2/29 13:05
 * @Version 1.0
 **/
@Slf4j
@Component
public class UserServiceImpl implements UserService {

    @Override
    public String getName() {
        log.info("熔断默认调用的函数！");
        return "请求异常，返回熔断方法！";
    }
}
