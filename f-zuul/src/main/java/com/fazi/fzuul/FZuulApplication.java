package com.fazi.fzuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.provider.authentication.BearerTokenExtractor;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationProcessingFilter;
import org.springframework.security.oauth2.provider.error.DefaultOAuth2ExceptionRenderer;
import org.springframework.security.web.access.ExceptionTranslationFilter;

@SpringBootApplication
@EnableZuulProxy
// 不用于服务发现等，可不加
@EnableEurekaClient
public class FZuulApplication {

    public static void main(String[] args) {
//        DefaultOAuth2ExceptionRenderer
//        ExceptionTranslationFilter
//        BearerTokenExtractor
//        OAuth2AuthenticationProcessingFilter
        SpringApplication.run(FZuulApplication.class, args);
    }
}
