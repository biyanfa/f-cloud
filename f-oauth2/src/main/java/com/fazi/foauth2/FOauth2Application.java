package com.fazi.foauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.provider.endpoint.AuthorizationEndpoint;
import org.springframework.security.oauth2.provider.endpoint.FrameworkEndpointHandlerMapping;

@SpringBootApplication
public class FOauth2Application {

    public static void main(String[] args) {
        SpringApplication.run(FOauth2Application.class, args);
    }

}
