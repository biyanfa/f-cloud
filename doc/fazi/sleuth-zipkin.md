### springCloud 从 F 版器已不需要自己构建 Zipkin Server 了，只需要调用 jar 包即可
### sleuth负责链路追踪，zipkin负责展现
### 1、下载 zipkin-server-2.20.2-exec.jar 并 java -jar zipkin-server-2.20.2-exec.jar 启动
### 2、在需要跟踪的服务上添加依赖
```
<!--        包含了 sleuth 和 zipkin-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-zipkin</artifactId>
</dependency>
```
### 3、启动服务，访问 http://192.168.1.21:9411/zipkin/，选择相关过滤条件，查询即可