package com.fazi.fprovide.service;

import com.fazi.fprovide.domain.SysUser;

/**
 * @InterfaceName SysUserService
 * @Description TODO
 * @Author fazi
 * @Date 2020/3/5 22:15
 * @Version 1.0
 **/
public interface SysUserService {

    SysUser selectByPrimaryKey(Long userId);
}
