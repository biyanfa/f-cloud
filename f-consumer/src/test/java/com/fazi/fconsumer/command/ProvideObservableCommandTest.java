package com.fazi.fconsumer.command;

import org.junit.jupiter.api.Test;
import rx.Observable;

import java.util.Iterator;

class ProvideObservableCommandTest {

    @Test
    public void testObservable() {
        Observable<String> observable= new ProvideObservableCommand().observe();
        Iterator<String> iterator = observable.toBlocking().getIterator();
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    @Test
    public void testToObservable() {
        Observable<String> observable= new ProvideObservableCommand().toObservable();
        Iterator<String> iterator = observable.toBlocking().getIterator();
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}