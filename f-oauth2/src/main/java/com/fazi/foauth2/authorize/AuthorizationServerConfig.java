package com.fazi.foauth2.authorize;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 * @ClassName AuthorizationServerConfig
 * @Description TODO
 * @Author fazi
 * @Date 2020/3/4 14:13
 * @Version 1.0
 **/
@Configuration
@EnableAuthorizationServer
@Slf4j
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    PasswordEncoder passwordEncoder;

    /**
     * 配置资源服务器过来验 token 的规则
     **/
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        // 过来验令牌有效性的请求，不是谁都能验的，必须要是经过身份认证的。
        // 所谓身份认证就是，必须携带clientId，clientSecret，否则随便一请求过来验token是不验的

        // 允许所有人请求token
        // 已验证的用户才能请求check_token端点
        security
                .tokenKeyAccess("permitAll()")
                .checkTokenAccess("isAuthenticated()")//isAuthenticated():排除anonymous   isFullyAuthenticated():排除anonymous以及remember-me
                .allowFormAuthenticationForClients()//允许表单认证  这段代码在授权码模式下会导致无法根据code　获取token　
        ;
    }

    /**
     * 配置客户端应用的信息，让认证服务器知道有哪些客户端应用来申请令牌
     **/
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        // TODO 多了的时候如何配置
        clients.inMemory()// 配置在内存里，后面改为数据库里
                .withClient("fazi")// 注册客户端应用，使客户端能够访问服务器，客户端访问了
                .secret("fazi")// 客户端安全码
                .redirectUris("http://f-zuul:8800/login")
                .scopes("all")// 授权范围：write、read等，多个以,号隔开，默认空不受限制
//                .accessTokenValiditySeconds(3600)// token 有效期
                .autoApprove()
                .authorizedGrantTypes("authorization_code", "implicit", "password", "client_credentials", "refresh-token")// 客户端可以使用的授权类型，参数是可变参数,多个参数表示支持多种类型
                // 自动授权，无需人工手动点击 approve
//                .autoApprove(true)// 为true 则不会被重定向到授权的页面，也不需要手动给请求授权,直接自动授权成功返回code
//                .resourceIds("f-provide", "provide")// 资源服务器的id。发给orderApp的token，能访问哪些资源服务器，可以多个
                ;
    }

    /**
     * 配置用户信息
     * 在UAA开源项目中，refresh token有两者使用模式：重复使用和非重复使用。
     * 所谓重复使用指的是登陆后初次生成的refresh token一直保持不变，直到过期；
     * 非重复使用指的是在每一次使用refresh token刷新access token的过程中，refresh token也随之更新，即生成新的refresh token。
     * 在UAA项目中这一设置是通过UaaConfiguration类reuseRefreshTokens(boolean b)(默认为true)来设置完成；
     **/
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        //传给他一个authenticationManager用来校验传过来的用户信息是不是合法的
        endpoints.tokenStore(new InMemoryTokenStore())
                .accessTokenConverter(accessTokenConverter())
                .authenticationManager(authenticationManager)
//                .userDetailsService(userDetailsService) //必须注入userDetailsService否则根据refresh_token无法加载用户信息
                .reuseRefreshTokens(false);
    }

//    @Slf4j
//    @Service(value = "userService")
//    public class UserServiceImpl implements UserDetailsService {
//
//        @Autowired
//        private SysAccountRepository repository;
//
//        public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//            SysAccount user = repository.findByUserAccount(username);
//            if(user == null){
//                log.info("登录用户【"+username + "】不存在.");
//                throw new UsernameNotFoundException("登录用户【"+username + "】不存在.");
//            }
//            return new org.springframework.security.core.userdetails.User(user.getUserAccount(), user.getUserPwd(), getAuthority());
//        }
//
//        private List getAuthority() {
//            return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
//        }
//
//
//    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter(){
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        // 签名秘钥
        converter.setSigningKey("fazi-secret");
        return converter;
    }

      // 还可以进行增强，enhance 在返回的信息中添加自定义ide内容
//    @Bean
//    public JwtAccessTokenConverter accessTokenConverter() {
//        JwtAccessTokenConverter converter = new JwtAccessTokenConverter() {
//            /**
//             * 自定义一些token返回的信息
//             * @param accessToken
//             * @param authentication
//             * @return
//             */
//            @Override
//            public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
//                String grantType = authentication.getOAuth2Request().getGrantType();
//                //只有如下两种模式才能获取到当前用户信息
//                if("authorization_code".equals(grantType) || "password".equals(grantType)) {
//                    String userName = authentication.getUserAuthentication().getName();
//                    // 自定义一些token 信息 会在获取token返回结果中展示出来
//                    final Map<String, Object> additionalInformation = new HashMap<>();
//                    additionalInformation.put("user_name", userName);
//                    ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInformation);
//                }
//                OAuth2AccessToken token = super.enhance(accessToken, authentication);
//                return token;
//            }
//        };
//        converter.setSigningKey("bcrypt");
//        return converter;
//    }

    // JWT令牌存储组件
    public JwtTokenStore jwtTokenStore(){
        return new JwtTokenStore(accessTokenConverter());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        // 5.0后被认为是不安全的，不带有 {id} 前缀，报错
        DelegatingPasswordEncoder delegatingPasswordEncoder =
                (DelegatingPasswordEncoder) PasswordEncoderFactories.createDelegatingPasswordEncoder();
        //设置defaultPasswordEncoderForMatches为NoOpPasswordEncoder
        delegatingPasswordEncoder.setDefaultPasswordEncoderForMatches(NoOpPasswordEncoder.getInstance());
        return  delegatingPasswordEncoder;
    }
}
