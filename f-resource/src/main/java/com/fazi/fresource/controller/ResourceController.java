package com.fazi.fresource.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.BearerTokenExtractor;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.Principal;

/**
 * @ClassName ResourceController
 * @Description TODO
 * @Author fazi
 * @Date 2020/3/15 14:57
 * @Version 1.0
 **/
@RestController
public class ResourceController {

    @Autowired
    ResourceServerTokenServices tokenServices;

    BearerTokenExtractor tokenExtractor = new BearerTokenExtractor();

    // 为了返回授权用户的信息，根据访问令牌获取用户认证信息的接口
    @GetMapping(value = "/user")
    public Principal userInfo(ServletRequest req) throws IOException {

        final HttpServletRequest request = (HttpServletRequest) req;
        Authentication authentication = tokenExtractor.extract(request);
        String token = (String) authentication.getPrincipal();
        OAuth2Authentication oAuth2Authentication = tokenServices.loadAuthentication(token);
        return oAuth2Authentication;
    }

    // 拦截资源
    @GetMapping(value = "/user/a")
    public String getUserByUserId() {
        return "拦截的资源";
    }

    // 不拦截资源
    @GetMapping(value = "/instance/a")
    public String getInstanceByServiceId() {
        return "不拦截的资源";
    }
}
