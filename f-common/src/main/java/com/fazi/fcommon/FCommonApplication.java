package com.fazi.fcommon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(FCommonApplication.class, args);
    }

}
