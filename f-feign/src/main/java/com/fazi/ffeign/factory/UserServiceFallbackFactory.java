package com.fazi.ffeign.factory;

import com.fazi.ffeign.service.FactoryUserService;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @ClassName UserServiceFallbackFactory
 * @Description 单体配置降级
 * @Author fazi
 * @Date 2020/3/3 12:31
 * @Version 1.0
 **/
@Component
public class UserServiceFallbackFactory implements FallbackFactory<FactoryUserService> {
    @Override
    public FactoryUserService create(Throwable throwable) {
        return new FactoryUserService() {
            @Override
            public String getName() {
                return "工厂 factory 降级";
            }
        };
    }
}
