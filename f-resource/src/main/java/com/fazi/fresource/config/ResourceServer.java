package com.fazi.fresource.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

/**
 * @ClassName ResourceServer
 * @Description TODO
 * @Author fazi
 * @Date 2020/3/6 22:47
 * @Version 1.0
 **/
@Configuration
@EnableResourceServer
public class ResourceServer extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.
                sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                .and()
                .requestMatchers().anyRequest()
                .and()
                .anonymous()
                .and()
                .authorizeRequests()
                .antMatchers("/user/**").authenticated() // /user/** 端点的访问必须要验证后
//                .antMatchers("/provide/**", "/f-provide/**", "/fazi/user").authenticated()
//                .antMatchers("/pwd").authenticated()
//                .antMatchers("getName").permitAll()
                .and()
                .exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
//            .exceptionHandling().accessDeniedHandler(customAccessDeniedHandler())  //权限认证失败业务处理
//                .authenticationEntryPoint(customAuthenticationEntryPoint());  //认证失败的业务处理
//        http.addFilterBefore(permitAuthenticationFilter,AbstractPreAuthenticatedProcessingFilter.class); //自定义token过滤 token校验失败后自定义返回数据格式

    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources
//                .tokenStore(new JwtTokenStore(accessTokenConverter()))
//                .stateless(true)
                .tokenServices(tokenServices())
                .stateless(true);// 访问无状态
    }

    @Bean
    public ResourceServerTokenServices tokenServices() {

        // 配置RemoteTokenServices，用于向AuththorizationServer验证token
        // 这里访问的是远程令牌
        RemoteTokenServices tokenServices = new RemoteTokenServices();
//        tokenServices.setAccessTokenConverter(accessTokenConverter());

        // 为restTemplate配置异常处理器，忽略400错误，
        getRestTemplate().setErrorHandler(new DefaultResponseErrorHandler() {
            @Override
            // Ignore 400
            public void handleError(ClientHttpResponse response) throws IOException {
                if (response.getRawStatusCode() != 400) {
                    super.handleError(response);
                }
            }
        });
        tokenServices.setRestTemplate(getRestTemplate());
        tokenServices.setCheckTokenEndpointUrl("http://f-oauth2:8810/oauth/check_token");
        tokenServices.setClientId("fazi");
        tokenServices.setClientSecret("fazi");
        return tokenServices;
    }

    @Bean
    @LoadBalanced// Spring Cloud Ribbon 是基于 Netflix Ribbon 实现的一套客户端
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter(){
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        // 签名秘钥
        converter.setSigningKey("fazi-secret");
        return converter;
    }
}
