package com.fazi.ffeign.config.custom;

import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.Server;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @ClassName MyRule
 * @Description TODO
 * @Author fazi
 * @Date 2020/2/28 13:09
 * @Version 1.0
 **/
public class MyRule implements IRule {

    private ILoadBalancer iLoadBalancer;

    @Override
    public Server choose(Object key) {
        List<Server> allServers = iLoadBalancer.getAllServers();
        //输出一遍提供者实例
        allServers.stream().forEach(server -> System.out.println(server.getHostPort()));
        if(CollectionUtils.isEmpty(allServers)){
            return null;
        }
        return allServers.get(0);
    }

    @Override
    public void setLoadBalancer(ILoadBalancer lb) {
        this.iLoadBalancer = lb;
    }

    @Override
    public ILoadBalancer getLoadBalancer() {
        return this.iLoadBalancer;
    }
}
