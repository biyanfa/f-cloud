package com.fazi.fconsumer.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

/**
 * @ClassName MessageController
 * @Description stream rabbitmq test
 * @Author fazi
 * @Date 2020/3/14 21:57
 * @Version 1.0
 **/
@Component
@EnableBinding(Sink.class)
@Slf4j
public class MessageController {

    @Value("${server.port}")
    private String port;

    @StreamListener(Sink.INPUT)
    public void input(Message<String> message){
        log.info("receive message is :" + message.getPayload() + " port is : " + port);
    }

}
