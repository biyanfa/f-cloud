
1. 使用此模块的时候需要在监控的服务上添加依赖
```
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>  
</dependency>  
<dependency>  
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
</dependency>
```
2. 本模块添加依赖
```
<dependency>
    <groupId>io.pivotal.spring.cloud</groupId>
    <artifactId>spring-cloud-services-starter-circuit-breaker</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-hystrix-dashboard</artifactId>
</dependency>
```
3. 启动类添加
```
// 此配置是为了服务监控而配置，与服务容错本身无关，
// ServletRegistrationBean因为springboot的默认路径不是"/hystrix.stream"，
// 只要在自己的项目里配置上下面的servlet就可以了
@Bean
public ServletRegistrationBean getServlet() {
    HystrixMetricsStreamServlet streamServlet = new HystrixMetricsStreamServlet();
    ServletRegistrationBean registrationBean = new ServletRegistrationBean(streamServlet);
    registrationBean.setLoadOnStartup(1);
    registrationBean.addUrlMappings("/actuator/hystrix.stream");
    registrationBean.setName("HystrixMetricsStreamServlet");
    return registrationBean;
}
```
4. Hystrix Dashboard共支持三种不同的监控方式：  
默认的集群监控： http://turbine-hostname:port/turbine.stream  
指定的集群监控： http://turbine-hostname:port/turbine.stream?cluster=\[clusterName\]  
单体应用的监控： http://hystrix-app:port/actuator/hystrix.stream

**页面上面的几个参数局域**  
>最上面的输入框： 输入上面所说的三种监控方式的地址，用于访问具体的监控信息页面。  
Delay： 该参数用来控制服务器上轮询监控信息的延迟时间，默认2000毫秒。  
Title： 该参数对应头部标题Hystrix Stream之后的内容，默认会使用具体监控实例的Url。

原文链接：https://blog.csdn.net/Afflatus_f/article/details/102791031

后面整合 Turbine 集群的时候再说明后两种的监控方式。

5. 如果使用springboot2.0 和spring cloud Finchley.M8 版本搭建 使用（/actuator/hystrix.stream  而不是/hystrix.stream 为插入点）