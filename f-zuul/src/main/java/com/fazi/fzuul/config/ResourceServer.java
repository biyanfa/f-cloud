//package com.fazi.fzuul.config;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.annotation.Order;
//import org.springframework.http.client.ClientHttpResponse;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.http.SessionCreationPolicy;
//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
//import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
//import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
//import org.springframework.security.oauth2.provider.authentication.BearerTokenExtractor;
//import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationProcessingFilter;
//import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
//import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
//import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
//import org.springframework.web.client.DefaultResponseErrorHandler;
//import org.springframework.web.client.RestTemplate;
//
//import java.io.IOException;
//
///**
// * @ClassName ResourceServer
// * @Description TODO
// * @Author fazi
// * @Date 2020/3/6 22:47
// * @Version 1.0
// **/
//@Configuration
//@EnableResourceServer
////@Order(1)
//public class ResourceServer extends ResourceServerConfigurerAdapter {
//
//    @Autowired
//    private RestTemplate restTemplate;
//
//    @Value("${security.oauth2.resource.token-info-uri}")
//    String tokenInfoUri;
//
//    @Value("${security.oauth2.client.client-id}")
//    String clientId;
//
//    @Value("${security.oauth2.client.client-secret}")
//    String secret;
//
//    @Override
//    public void configure(HttpSecurity http) throws Exception {
////        http
////                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
////                .and()
////                .authorizeRequests()
////                .antMatchers("/login").permitAll()
////                .antMatchers("/**")
////                .authenticated()
////                .and()
////                .logout().logoutUrl("/login")
////        ;
//        http.
//                sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
//                .and()
//                .requestMatchers().anyRequest()
//                .and()
//                .anonymous()
//                .and()
//                .authorizeRequests()
//                .antMatchers("/user/**").authenticated() // /user/** 端点的访问必须要验证后
//                .antMatchers("/provide/**", "/f-provide/**", "/fazi/user").authenticated()
//                .antMatchers("/pwd").authenticated()
//                .antMatchers("getName").permitAll()
//                .and()
//                .exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
////            .exceptionHandling().accessDeniedHandler(customAccessDeniedHandler())  //权限认证失败业务处理
////                .authenticationEntryPoint(customAuthenticationEntryPoint());  //认证失败的业务处理
////        http.addFilterBefore(permitAuthenticationFilter,AbstractPreAuthenticatedProcessingFilter.class); //自定义token过滤 token校验失败后自定义返回数据格式
//
//    }
//
//    @Override
//    public void configure(ResourceServerSecurityConfigurer resources) {
//        resources.tokenServices(tokenServices()).stateless(true);
//    }
//
//    @Bean
//    public ResourceServerTokenServices tokenServices() {
//
//        // 配置RemoteTokenServices，用于向AuththorizationServer验证token
//        RemoteTokenServices tokenServices = new RemoteTokenServices();
////        tokenServices.setAccessTokenConverter(accessTokenConverter());
//
//        // 为restTemplate配置异常处理器，忽略400错误，
//        restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
//            @Override
//            // Ignore 400
//            public void handleError(ClientHttpResponse response) throws IOException {
//                if (response.getRawStatusCode() != 400) {
//                    super.handleError(response);
//                }
//            }
//        });
//        tokenServices.setRestTemplate(restTemplate);
//        tokenServices.setCheckTokenEndpointUrl(tokenInfoUri);
//        tokenServices.setClientId(clientId);
//        tokenServices.setClientSecret(secret);
//        return tokenServices;
//    }
//}
