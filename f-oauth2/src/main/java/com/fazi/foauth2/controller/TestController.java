package com.fazi.foauth2.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName TestController
 * @Description TODO
 * @Author fazi
 * @Date 2020/3/5 12:47
 * @Version 1.0
 **/
@RestController
public class TestController {

    @GetMapping(value = "/test/info")
    public String getTestInfo(){
        return "测试案例";
    }

    @GetMapping
    public String getHome(){
        return "Welcome to Oauth2!";
    }
}
