# 一、手动刷新
##  1、在client端添加依赖，让别人能够监控到自己的变化
```
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```
## 2、在配置文件中添加配置
```
#暴露监控端点，用于手动刷新使用management:
endpoints:
web:
  exposure:
    include: *
```
## 3、在业务类上添加 @RefreshScope 注解
## 4、配置信息变更人员在配置变化后，发送通知到 config-client
curl -X POST "http://localhost:8801/actuator/refresh"
## 说明
这种情况运维挺累的啊
# 二、自动刷新 Bus RabbitMQ
##  1、server 端
#### 1.1、在 server 端添加依赖
```
<!-- 添加消息总线 rabbitMq 支持 -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-bus-amqp</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```
#### 1.2、然后在 application.yml 文件中增加配置信息
```
# rabbitmq 相关配置
spring:
     rabbitmq:
        host: localhost
        port: 5672
        username: guest
        password: guest
# bus 刷新地址暴露
management:
    endpoints:
        web:
            exposure:
                include: 'bus-refresh'
```
#### 1.3、补充说明
```
4369 -- erlang发现口
5672 --client端通信口
15672 -- 管理界面ui端口
25672 -- server间内部通信口
```
## 2、客户端
#### 2.1、增加依赖
```
<!-- 添加消息总线 rabbitMq 支持 -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-bus-amqp</artifactId>
</dependency>
```
#### 1.2、然后在 application.yml 或者 bootsrap.yml 文件中增加配置信息
```
# rabbitmq 相关配置，注意这里是spring下边的
spring: 
    rabbitmq:
        host: localhost
        port: 5672
        username: guest
        password: guest
```
#### 1.3、配置信息变更人员在配置变化后，发送通知到 config-server
```
curl -X POST "http://localhost:8820/actuator/bus-refresh"
```
一处发送，处处生效
## 3 过滤刷新
```
# 只通知3355
curl -X POST "http://localhost:8820/actuator/bus-refresh/config-client:3355"
```


# 在多种消息中间件之间切换的消息驱动框架，见 cloud stream.md