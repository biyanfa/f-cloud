package com.fazi.fprovide.service.impl;

import com.fazi.fprovide.service.IMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * @ClassName IMessageServiceImpl
 * @Description TODO
 * @Author fazi
 * @Date 2020/3/14 16:35
 * @Version 1.0
 **/
// 可以理解为是一个消息的发送管道的定义
@EnableBinding(Source.class)
@Slf4j
public class IMessageServiceImpl implements IMessageService {

    // 消息的发送管道
    @Resource
    private MessageChannel output;

    @Override
    public String send() {
        String uuid = UUID.randomUUID().toString();
        output.send(MessageBuilder.withPayload(uuid).build());
        log.info("send message is :" + uuid);
        return null;
    }
}
