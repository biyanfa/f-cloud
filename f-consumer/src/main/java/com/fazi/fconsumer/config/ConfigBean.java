package com.fazi.fconsumer.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @ClassName ConfigBean
 * @Description TODO
 * @Author fazi
 * @Date 2020/2/25 23:01
 * @Version 1.0
 **/
@Configuration
public class ConfigBean {

    @Bean
    @LoadBalanced// Spring Cloud Ribbon 是基于 Netflix Ribbon 实现的一套客户端
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }
}
