package com.fazi.fconsumer.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.ObservableExecutionMode;
import com.netflix.hystrix.contrib.javanica.command.AsyncResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.authentication.BearerTokenExtractor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import rx.Observable;
import rx.Subscriber;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

/**
 * @ClassName ConsumerController
 * @Description TODO
 * @Author fazi
 * @Date 2020/2/25 23:02
 * @Version 1.0
 **/
@RestController
public class ConsumerController {

    @Autowired
    private RestTemplate restTemplate;

    private static final String REST_URL_PREFIX = "http://f-provide";

    @GetMapping(value = "/getName")
    public String getName(){
        return "你好啊，授权了";
    }

    @GetMapping(value = "/getRest/{name}")
    public String getRest(@PathVariable("name") String name){
//        return restTemplate.getForEntity(REST_URL_PREFIX + "/fazi/get/{1}", String.class, new Object[] {name}).getBody();
//        Map<String, Object> map = new HashMap<String, Object>();
//        map.put("name", name);
//        return restTemplate.getForEntity(REST_URL_PREFIX + "/fazi/get/{name}", String.class, map).getBody();
//        return restTemplate.getForEntity(REST_URL_PREFIX + "/fazi/get/{1}", String.class, name).getBody();
        return restTemplate.getForObject(REST_URL_PREFIX + "/fazi/get/" + name, String.class);
    }

    /**
     * groupKey: 设置HystrixCommand分组的名称
     * commandKey: 设置HystrixCommand的名称
     * threadPollKey: 设置HystrixCommand执行线程池的名称
     * fallbackMethod: 设置HystrixCommand服务降级所使用的方法名称，注意该方法需要与原方法定义在同一个类中，并且方法签名也要一致
     * commandProperties: 设置HystrixCommand属性，如：断路器失败百分比、断路器时间容器大小等
     * ignoreException: 设置HystrixCommand执行服务降级处理时需要忽略的异常，当出现异常时不会执行服务降级处理。
     * observableExecutionMode: 设置HystrixCommand执行的方式
     * defaultFallback: 设置HystrixCommand默认的服务降级处理方法，如果同时设定了fallbackMethod，会优先使用fallbackMethod所指定的方法，需要注意的是defaultFallback该属性所指定的方法没有参数，需要注意返回值与原方法返回值的兼容性
     **/
    // 同步的方式
    @HystrixCommand(fallbackMethod = "fallBack")
    @GetMapping(value = "/getHystrix/{name}")
    public String getHystrix(@PathVariable("name") String name){
        return restTemplate.getForObject(REST_URL_PREFIX + "/fazi/get/" + name, String.class);
    }

    public String fallBack(@PathVariable("name") String name){
        return "简单服务降级实现！";
    }

    // 熔断异步的执行
    @HystrixCommand(fallbackMethod = "asyncFallback")
    @GetMapping(value = "/getAsyncHystrix/{name}")
    public Future<String> getAsyncHystrix(@PathVariable("name") String name) {
        return new AsyncResult<String>() {
            @Override
            public String invoke() {
                int i = 1/0;//此处抛异常,测试服务降级
                return "name:" + name;
            }
        };
    }

    public String asyncFallback(@PathVariable("name") String name){
        return "简单服务降级实现！--异步";
    }

    /**
     *  EAGER参数表示使用observe()方式执行
     */
    @HystrixCommand(observableExecutionMode = ObservableExecutionMode.EAGER, fallbackMethod = "observFailed") //使用observe()执行方式
    public Observable<String> getUserById(final Long id) {
        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                try {
                    if(!subscriber.isUnsubscribed()) {
                        subscriber.onNext("张三的ID:");
                        int i = 1 / 0; //抛异常，模拟服务降级
                        subscriber.onNext(String.valueOf(id));
                        subscriber.onCompleted();
                    }
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    private String observFailed(Long id) {
        return "observFailed---->" + id;
    }

    /**
     * LAZY参数表示使用toObservable()方式执行
     */
    @HystrixCommand(observableExecutionMode = ObservableExecutionMode.LAZY, fallbackMethod = "toObserbableError") //表示使用toObservable()执行方式
    public Observable<String> getUserByName(final String name) {
        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                try {
                    if(!subscriber.isUnsubscribed()) {
                        subscriber.onNext("找到");
                        subscriber.onNext(name);
                        int i = 1/0; ////抛异常，模拟服务降级
                        subscriber.onNext("了");
                        subscriber.onCompleted();
                    }
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    private String toObserbableError(String name) {
        return "toObserbableError--->" + name;
    }
}
