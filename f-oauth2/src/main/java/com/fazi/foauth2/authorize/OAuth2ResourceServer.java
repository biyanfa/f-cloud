//package com.fazi.foauth2.authorize;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
//import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
//
///**
// * @ClassName OAuth2ResourceServer
// * @Description 资源服务器
// * @Author fazi
// * @Date 2020/3/4 21:37
// * @Version 1.0
// **/
//@Configuration
//@EnableResourceServer
//public class OAuth2ResourceServer extends ResourceServerConfigurerAdapter {
//
//    @Override
//    public void configure(HttpSecurity http) throws Exception {
//        http.authorizeRequests()
//                .anyRequest()
//                .authenticated()
//                .and()
//                .requestMatchers()
//                .antMatchers("/test/**");
//    }
//
//}
