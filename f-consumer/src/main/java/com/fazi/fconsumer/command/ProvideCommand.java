package com.fazi.fconsumer.command;

import com.netflix.hystrix.*;
import com.netflix.hystrix.strategy.concurrency.HystrixConcurrencyStrategyDefault;
import com.netflix.hystrix.strategy.properties.HystrixPropertiesCommandDefault;

/**
 * @ClassName ProvideCommand
 * @Description HystrixCommand用在命令服务返回单个操作结果的时候
 * 两种执行方式
 * 　　  -execute():同步执行。从依赖的服务返回一个单一的结果对象，或是在发生错误的时候抛出异常。
 * 　　  -queue();异步执行。直接返回一个Future对象，其中包含了服务执行结束时要返回的单一结果对象。
 * @Author fazi
 * @Date 2020/3/3 11:24
 * @Version 1.0
 **/
public class ProvideCommand extends HystrixCommand<String> {

    /**
     * Hystrix每个command都有对应的commandKey可以认为是command的名字，
     * 默认是当前类的名字getClass().getSimpleName(),
     * 每个command也都一个归属的分组，这两个东西主要方便Hystrix进行监控、报警等。
     * HystrixCommand使用的线程池也有线程池key，以及对应线程相关的配置
     **/
    protected ProvideCommand() {
        // Hystrix command配置有熔断阀值，熔断百分比等配置，ThreadPoll有线程池大小，队列大小等配置
//        super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("test")));
        // 可以参考 https://github.com/Netflix/Hystrix/wiki/Configuration
        super(Setter
                //分组key
                .withGroupKey(HystrixCommandGroupKey.Factory.asKey("helloWorldGroup"))

                //commandKey
                .andCommandKey(HystrixCommandKey.Factory.asKey("commandHelloWorld"))
                //command属性配置
                .andCommandPropertiesDefaults(HystrixPropertiesCommandDefault.Setter().withCircuitBreakerEnabled(true).withCircuitBreakerForceOpen(true))

                //线程池key
                .andThreadPoolKey(HystrixThreadPoolKey.Factory.asKey("helloWorld_Poll"))
                //线程池属性配置
                .andThreadPoolPropertiesDefaults(HystrixThreadPoolProperties.Setter().withCoreSize(20).withMaxQueueSize(25)));
    }

    // 实现具体的业务处理逻辑
    @Override
    protected String run() throws Exception {
        return "HystrixCommand 服务";
    }

    /**
     * 降级。Hystrix会在run()执行过程中出现错误、超时、线程池拒绝、断路器熔断等情况时，
     * 执行getFallBack()方法内的逻辑
     */
    @Override
    protected String getFallback() {
        return "HystrixCommand 服务降级";
    }

    //
    /**
     * 清理缓存使用
     * HystrixRequestContext context = HystrixRequestContext.initializeContext();
     * ....... command 的调用 ........
     * context.shutdown();
     **/
    public static void flushCache(String key) {
        HystrixRequestCache.getInstance(HystrixCommandKey.Factory.asKey("commandHelloWorld"),
                HystrixConcurrencyStrategyDefault.getInstance()).clear(key);
    }
}
