## 1、屏蔽底层消息中间件的差异，降低切换成本，同一消息的编程模型
目前支持：RabbitMQ 和 kafka
## 2、原理
Binder 隔离
## 3、实现
#### 3.1、provider
##### 3.1.1 增加依赖
```
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-stream-rabbit</artifactId>
</dependency>
```
##### 3.1.2 添加 application.yml 配置信息
```
# stream 相关配置
spring:
  cloud:
    stream:
      binders: # 在此处配置要绑定的 rabbitmq 的服务信息
        defaultRabbit: # 表示定义的名称，用于和 binding整合
          type: rabbit # 消息组件类型
          environment: # 设置 rabbitmq 的相关的环境配置
            spring:
              rabbitmq:
                host: 192.168.1.21
                port: 5672
                username: guest
                password: guest
      bindings: # 服务的整合处理
        output: # 这个名字是一个通道的名称
          destination: faziExchange # 表示要使用的 Exchange 名称定义
          content-type: application/json # 设置消息类型，本次为 json ，文本则设置"text/plain"
          binder: defaultRabbit # 设置要绑定的消息服务的具体设置，也就是上边 binders 中定义的名称
```
##### 3.1.3 添加类
```
// 可以理解为是一个消息的发送管道的定义
@EnableBinding(Source.class)
@Slf4j
public class IMessageServiceImpl implements IMessageService {

    // 消息的发送管道
    @Resource
    private MessageChannel output;

    @Override
    public String send() {
        String uuid = UUID.randomUUID().toString();
        output.send(MessageBuilder.withPayload(uuid).build());
        log.info("send message is :" + uuid);
        return null;
    }
}
```
#### 3.2 consumer
##### 3.2.1 增加依赖
```
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-stream-rabbit</artifactId>
</dependency>
```
##### 3.2.2 添加 application.yml 配置信息
```
# stream 相关配置
spring:
  cloud:
    stream:
      binders: # 在此处配置要绑定的 rabbitmq 的服务信息
        defaultRabbit: # 表示定义的名称，用于和 binding整合
          type: rabbit # 消息组件类型
          environment: # 设置 rabbitmq 的相关的环境配置
            spring:
              rabbitmq:
                host: 192.168.1.21
                port: 5672
                username: guest
                password: guest
      bindings: # 服务的整合处理
        input: # 这个名字是一个通道的名称
          destination: faziExchange # 表示要使用的 Exchange 名称定义
          content-type: application/json # 设置消息类型，本次为 json ，文本则设置"text/plain"
          binder: defaultRabbit # 设置要绑定的消息服务的具体设置，也就是上边 binders 中定义的名称
```
## provider 和 consumer 的不同点是 bindings 下的 output 和 input
##### 3.2.3 增加类监听
```
@Component
@EnableBinding(Sink.class)
@Slf4j
public class MessageController {

    @Value("${server.port}")
    private String port;

    @StreamListener(Sink.INPUT)
    public void input(Message<String> message){
        log.info("receive message is :" + message.getPayload() + " port is : " + port);
    }

}
```
## 4 重复消费问题和持久化问题
问题产生原因：如订单服务只能有一个消费方（一个生产者对应多个消费方的时候，有时需求是重复消费）
在配置文件中的 input 下增加人如下配置
```
          # 如果没有显示指定 group 会发生消息丢失的问题，即在 down 的情况下，未曾接收的消息会丢失，不会消费
          group: f-consumer # 自定义分组（流水号），避免重复消费问题，同分组是竞争关系，轮训消费，不同组可以重复消费，当消费者多个时，可指定相同分组，避免重复消费
```