package com.fazi.fprovide.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName UserContoller
 * @Description TODO
 * @Author fazi
 * @Date 2020/2/28 14:00
 * @Version 1.0
 **/
@RestController
public class UserContoller {

    @Value("${server.port}")
    String port;

    @Value("${f-user}")
    String fUser;

    @GetMapping(value = "/fazi/get/{name}")
    public String get(@PathVariable("name") String name){
        return "你好啊" + name + ", port:" + port;
    }

    @GetMapping(value = "/fazi/user")
    public String getName(){
        return "你好啊! port:" + port;
    }

    @GetMapping(value = "/fazi/fuser")
    public String getFUser(){
        return "你好啊! fUser:" + fUser;
    }
}
