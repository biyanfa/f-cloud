package com.fazi.fprovide.controller;

import com.fazi.fprovide.dao.SysUserDao;
import com.fazi.fprovide.dao.TestFazi;
import com.fazi.fprovide.domain.SysUser;
import com.fazi.fprovide.service.SysUserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.annotation.Resources;
import java.util.List;
import java.util.Map;

/**
 * @ClassName SysUserController
 * @Description TODO
 * @Author fazi
 * @Date 2020/3/5 17:54
 * @Version 1.0
 **/
@RestController
@RequestMapping(value = "/user")
public class SysUserController {

    @Autowired
    SysUserDao sysUserDao;

    @GetMapping
    SysUser selectByPrimaryKey(@RequestParam Long userId){
        return sysUserDao.selectByPrimaryKey(userId);
    }

    @GetMapping(value = "/queryInfos")
    List<SysUser> listInfos(@RequestParam Map<String, Object> param){
        return sysUserDao.listInfos(param);
    }

    @PostMapping
    int insert(@RequestBody SysUser record){
        return sysUserDao.insert(record);
    }

    @PostMapping(value = "/batchSave")
    int insertSelective(@RequestBody SysUser record){
        return sysUserDao.insertSelective(record);
    }

    @PutMapping(value = "/batchUpdate")
    int updateByPrimaryKeySelective(@RequestBody SysUser record){
        return sysUserDao.updateByPrimaryKeySelective(record);
    }

    @PutMapping
    int updateByPrimaryKey(@RequestBody SysUser record){
        return sysUserDao.updateByPrimaryKey(record);
    }

    @DeleteMapping
    int deleteByPrimaryKey(@RequestParam Long userId){
        return sysUserDao.deleteByPrimaryKey(userId);
    }
}
