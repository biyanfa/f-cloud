package com.fazi.feureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class FEurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(FEurekaApplication.class, args);
    }

}
