package com.fazi.ffeign.config;

import com.fazi.ffeign.config.custom.MyRule;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName RibbonConfig
 * @Description 通过 Bean 注册的方式配置 ribbon 策略
 * @Author fazi
 * @Date 2020/2/28 1:13
 * @Version 1.0
 **/
@Configuration
public class RibbonConfig {

    @Bean
    public IRule ribbonRule(){
//        return new MyRule();
        return new RandomRule();
    }
}
