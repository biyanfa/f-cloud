package com.fazi.fprovide;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@EnableDiscoveryClient
@SpringBootApplication
// 很神奇的啊，一开始没有引用不到 @Mapper
//@MapperScan(value = "com.fazi.fprovide.dao")
public class FProvideApplication {

    public static void main(String[] args) {
        SpringApplication.run(FProvideApplication.class, args);
    }
}