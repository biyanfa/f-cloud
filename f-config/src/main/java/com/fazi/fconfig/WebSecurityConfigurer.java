//package com.fazi.fconfig;//package com.fazi.fzuul.config;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.annotation.Order;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.builders.WebSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.config.http.SessionCreationPolicy;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.provisioning.InMemoryUserDetailsManager;
//
///**
// * @ClassName WebSecurityConfigurer
// * @Description
// * Spring Security默认是禁用注解的，要想开启注解，需要在继承WebSecurityConfigurerAdapter的类上加@EnableGlobalMethodSecurity注解。
// * @EnableGlobalMethodSecurity详解
// * @EnableGlobalMethodSecurity(securedEnabled=true) 开启@Secured 注解过滤权限
// * @EnableGlobalMethodSecurity(jsr250Enabled=true)开启@RolesAllowed 注解过滤权限 
// * @EnableGlobalMethodSecurity(prePostEnabled=true) 使用表达式时间方法级别的安全性         4个注解可用
// * @PreAuthorize 在方法调用之前, 基于表达式的计算结果来限制对方法的访问
// * @PostAuthorize 允许方法调用, 但是如果表达式计算结果为false, 将抛出一个安全性异常
// * @PostFilter 允许方法调用, 但必须按照表达式来过滤方法的结果
// * @PreFilter 允许方法调用, 但必须在进入方法之前过滤输入值
// * @Author fazi
// * @Date 2020/3/4 21:02
// * @Version 1.0
// **/
//
//@Configuration
//@EnableWebSecurity
////@EnableGlobalAuthentication
//@Slf4j
//@Order(2)
//public class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {
//
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        // 允许访问 /oauth 授权接口
//        http.csrf().disable()
//                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
//                .and()
//                .requestMatchers()
//                .anyRequest()
//                .and()
//                // 配置登陆页/login并允许访问
//                .formLogin().permitAll()
//                .and()
//                .authorizeRequests()
//                .antMatchers("/oauth/*", "/login")
//                .permitAll()
//                  登出页
//                  .and().logout().logoutUrl("/logout").logoutSuccessUrl("/")
//                .antMatchers("/test/**").authenticated()// 必须经过验证
//                .antMatchers("/fazi/user").authenticated()
//                .antMatchers("/provide/fazi/user").authenticated()
////                .and().csrf().disable()// 禁用csrf，jwt的时候不需要，禁用就好
//                ;
//
//
//    }
//}
