package com.fazi.fconfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class FConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(FConfigApplication.class, args);
    }

}
