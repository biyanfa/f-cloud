package com.fazi.fzuul.controller;
import	java.security.Principal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName MainController
 * @Description TODO
 * @Author fazi
 * @Date 2020/3/15 16:13
 * @Version 1.0
 **/
@RestController
public class MainController {

    @GetMapping
    public String main() {
        return "welcome to / index!";
    }

    @GetMapping(value = "/index")
    public String index() {
        return "welcome to index!";
    }

//    @GetMapping(value = "/login")
//    public String login() {
//        return "welcome to login!";
//    }

    // 返回当前的登录用户
    @GetMapping(value = "/user")
    public Principal user(Principal principal) {
        return principal;
    }
}
