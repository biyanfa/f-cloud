package com.fazi.ffeign.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @ClassName GlobalExceptionHandler
 * @Description TODO
 * @Author fazi
 * @Date 2020/3/3 12:39
 * @Version 1.0
 **/
@ControllerAdvice
public class GlobalExceptionHandler {

    // 全局异常处理
    @Resource
    private HttpServletResponse response;
    @ExceptionHandler(value =MyException.class)
    public void exceptionHandler(Exception e) throws IOException {
        System.out.println("系统错误！原因是:"+e);
        PrintWriter writer = response.getWriter();
        writer.print("service not available");
    }
}
