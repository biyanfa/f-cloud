package com.fazi.fconsumer.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

class ConsumerControllerTest {

    @Autowired
    ConsumerController consumerController;

    @Test
    void getHystrix() {
        consumerController.getHystrix("sync");
    }

    @Test
    void getAsyncHystrix() {
        consumerController.getAsyncHystrix("async");
    }

    @Test
    public void testObserve() {
        Iterator<String> iterator = consumerController.getUserById(30L).toBlocking().getIterator();
        while(iterator.hasNext()) {
            System.out.println("===============" + iterator.next());
        }
    }

    @Test
    public void testToObservable() {
        Iterator<String> iterator = consumerController.getUserByName("王五").toBlocking().getIterator();
        while(iterator.hasNext()) {
            System.out.println("===============" + iterator.next());
        }
    }
}