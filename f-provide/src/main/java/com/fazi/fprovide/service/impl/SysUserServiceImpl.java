package com.fazi.fprovide.service.impl;

import com.fazi.fprovide.dao.SysUserDao;
import com.fazi.fprovide.domain.SysUser;
import com.fazi.fprovide.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ClassName SysUserServiceImpl
 * @Description TODO
 * @Author fazi
 * @Date 2020/3/5 22:16
 * @Version 1.0
 **/
@Service
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    SysUserDao sysUserDao;

    @Override
    public SysUser selectByPrimaryKey(Long userId) {
        return sysUserDao.selectByPrimaryKey(userId);
    }
}
