package com.fazi.fprovide.controller;

import com.fazi.fprovide.service.IMessageService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @ClassName Message
 * @Description TODO
 * @Author fazi
 * @Date 2020/3/14 21:23
 * @Version 1.0
 **/
@RestController
public class MessageController {

    @Resource
    private IMessageService iMessageService;

    @GetMapping("/sendMessage")
    public String sendMessage(){
        iMessageService.send();
        return "成功";
    }
}
