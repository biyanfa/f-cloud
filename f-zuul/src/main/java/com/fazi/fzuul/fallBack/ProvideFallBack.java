//package com.fazi.fzuul.fallBack;
//import java.io.ByteArrayInputStream;
//import	java.net.HttpURLConnection;
//
//import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.client.ClientHttpResponse;
//import org.springframework.stereotype.Component;
//
//import java.io.IOException;
//import java.io.InputStream;
//
///**
// * @ClassName ProvideFallBack
// * @Description Zuul 服务降级
// * @Author fazi
// * @Date 2020/3/2 0:16
// * @Version 1.0
// **/
//@Component
//public class ProvideFallBack implements FallbackProvider {
//
//    // 设置好处理的失败路由
//    @Override
//    public String getRoute() {
//        // 或者 * 匹配所有
//        return "f-provide";
//    }
//
//    @Override
//    public ClientHttpResponse fallbackResponse(String route, Throwable cause) {
//        return new ClientHttpResponse() {
//            @Override
//            public HttpStatus getStatusCode() throws IOException {
//                return HttpStatus.BAD_REQUEST;
//            }
//
//            @Override
//            public int getRawStatusCode() throws IOException {
//                return HttpStatus.BAD_REQUEST.value();
//            }
//
//            @Override
//            public String getStatusText() throws IOException {
//                return HttpStatus.BAD_REQUEST.getReasonPhrase();
//            }
//
//            @Override
//            public void close() {
//
//            }
//
//            // 当出现服务调用错误之后返回的数据内容
//            @Override
//            public InputStream getBody() throws IOException {
//                return new ByteArrayInputStream("zuul 返回给消费方的错误信息！".getBytes());
//            }
//
//            @Override
//            public HttpHeaders getHeaders() {
//                HttpHeaders headers = new HttpHeaders() ;
//                headers.set("Content-Type", "application/json; charset=UTF-8");
//                return headers;
//            }
//        };
//    }
//}
