package com.fazi.fprovide.service;

/**
 * @InterfaceName IMessageService
 * @Description stream rabbitmq 测试
 * @Author fazi
 * @Date 2020/3/14 16:34
 * @Version 1.0
 **/
public interface IMessageService {

    public String send();
}
