# [网关+权限认证](https://gitee.com/hans_bstek/h-cloud/tree/oauth)
## 一、OAUTH2 基本概念
> ### 1、名词定义
> * Third-party application：第三方应用程序，本文中又称"客户端"（client），即上一节例子中的"云冲印"。
> * HTTP service：HTTP服务提供商，本文中简称"服务提供商"，即上一节例子中的Google。
> * Resource Owner：资源所有者，本文中又称"用户"（user）。
> * User Agent：用户代理，本文中就是指浏览器。
> * Authorization server：认证服务器，即服务提供商专门用来处理认证的服务器。
> * Resource server：资源服务器，即服务提供商存放用户生成的资源的服务器。它与认证服务器，可以是同一台服务器，也可以是不同的服务器。

> ### 2、OAuth的思路
> OAuth在"客户端"与"服务提供商"之间，设置了一个授权层（authorization layer）。"客户端"不能直接登录"服务提供商"，只能登录授权层，以此将用户与客户端区分开来。"客户端"登录授权层所用的令牌（token），与用户的密码不同。用户可以在登录的时候，指定授权层令牌的权限范围和有效期。
"客户端"登录授权层以后，"服务提供商"根据令牌的权限范围和有效期，向"客户端"开放用户储存的资料。

> ### 3、运行流程
>  #### OAuth 2.0的运行流程如下图:

![OAuth 2.0的运行流程](/resource/auth1.png)
> * （A）用户打开客户端以后，客户端要求用户给予授权。
> * （B）用户同意给予客户端授权。
> * （C）客户端使用上一步获得的授权，向认证服务器申请令牌。
> * （D）认证服务器对客户端进行认证以后，确认无误，同意发放令牌。
> * （E）客户端使用令牌，向资源服务器申请获取资源。
> * （F）资源服务器确认令牌无误，同意向客户端开放资源。
> <br> 不难看出来，上面六个步骤之中，B是关键，即用户怎样才能给于客户端授权。有了这个授权以后，客户端就可以获取令牌，进而凭令牌获取资源。

> 下面一一讲解客户端获取授权的四种模式。

> ### 4、客户端的授权模式
>  客户端必须得到用户的授权（authorization grant），才能获得令牌（access token）。OAuth 2.0定义了四种授权方式。
>
>   * 授权码模式（authorization code）
>   * 简化模式（implicit）
>   * 密码模式（resource owner password credentials）
>   * 客户端模式（client credentials）

> #### 4.1 授权码模式
> 授权码模式（authorization code）是功能最完整、流程最严密的授权模式。它的特点就是通过客户端的后台服务器，与"服务提供商"的认证服务器进行互动。
它的步骤如下：

![OAuth 2.0的运行流程](/resource/auth2.png)

> * （A）用户访问客户端，后者将前者导向认证服务器。
> * （B）用户选择是否给予客户端授权。
> * （C）假设用户给予授权，认证服务器将用户导向客户端事先指定的"重定向URI"（redirection URI），同时附上一个授权码。
> * （D）客户端收到授权码，附上早先的"重定向URI"，向认证服务器申请令牌。这一步是在客户端的后台的服务器上完成的，对用户不可见。
> * （E）认证服务器核对了授权码和重定向URI，确认无误后，向客户端发送访问令牌（access token）和更新令牌（refresh token）。

```
 下面是上面这些步骤所需要的参数。

A步骤中，客户端申请认证的URI，包含以下参数：

response_type：表示授权类型，必选项，此处的值固定为"code"
client_id：表示客户端的ID，必选项
redirect_uri：表示重定向URI，可选项
scope：表示申请的权限范围，可选项
state：表示客户端的当前状态，可以指定任意值，认证服务器会原封不动地返回这个值。
下面是一个例子。


GET /authorize?response_type=code&client_id=s6BhdRkqt3&state=xyz
        &redirect_uri=https%3A%2F%2Fclient%2Eexample%2Ecom%2Fcb HTTP/1.1
Host: server.example.com

C步骤中，服务器回应客户端的URI，包含以下参数：

code：表示授权码，必选项。该码的有效期应该很短，通常设为10分钟，客户端只能使用该码一次，否则会被授权服务器拒绝。该码与客户端ID和重定向URI，是一一对应关系。
state：如果客户端的请求中包含这个参数，认证服务器的回应也必须一模一样包含这个参数。
下面是一个例子。


HTTP/1.1 302 Found
Location: https://client.example.com/cb?code=SplxlOBeZQQYbYS6WxSbIA
          &state=xyz

D步骤中，客户端向认证服务器申请令牌的HTTP请求，包含以下参数：

grant_type：表示使用的授权模式，必选项，此处的值固定为"authorization_code"。
code：表示上一步获得的授权码，必选项。
redirect_uri：表示重定向URI，必选项，且必须与A步骤中的该参数值保持一致。
client_id：表示客户端ID，必选项。
下面是一个例子。


POST /token HTTP/1.1
Host: server.example.com
Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
Content-Type: application/x-www-form-urlencoded

grant_type=authorization_code&code=SplxlOBeZQQYbYS6WxSbIA
&redirect_uri=https%3A%2F%2Fclient%2Eexample%2Ecom%2Fcb

E步骤中，认证服务器发送的HTTP回复，包含以下参数：

access_token：表示访问令牌，必选项。
token_type：表示令牌类型，该值大小写不敏感，必选项，可以是bearer类型或mac类型。
expires_in：表示过期时间，单位为秒。如果省略该参数，必须其他方式设置过期时间。
refresh_token：表示更新令牌，用来获取下一次的访问令牌，可选项。
scope：表示权限范围，如果与客户端申请的范围一致，此项可省略。
下面是一个例子。


     HTTP/1.1 200 OK
     Content-Type: application/json;charset=UTF-8
     Cache-Control: no-store
     Pragma: no-cache

     {
       "access_token":"2YotnFZFEjr1zCsicMWpAA",
       "token_type":"example",
       "expires_in":3600,
       "refresh_token":"tGzv3JOkF0XG5Qx2TlKWIA",
       "example_parameter":"example_value"
     }

从上面代码可以看到，相关参数使用JSON格式发送（Content-Type: application/json）。此外，HTTP头信息中明确指定不得缓存。
```
> #### 4.2 简化模式
> 简化模式（implicit grant type）不通过第三方应用程序的服务器，直接在浏览器中向认证服务器申请令牌，跳过了"授权码"这个步骤，因此得名。所有步骤在浏览器中完成，令牌对访问者是可见的，且客户端不需要认证。

![OAuth 2.0的运行流程](/resource/auth3.png)

> 它的步骤如下：
> * （A）客户端将用户导向认证服务器。
> * （B）用户决定是否给于客户端授权。
> * （C）假设用户给予授权，认证服务器将用户导向客户端指定的"重定向URI"，并在URI的Hash部分包含了访问令牌。
> * （D）浏览器向资源服务器发出请求，其中不包括上一步收到的Hash值。
> * （E）资源服务器返回一个网页，其中包含的代码可以获取Hash值中的令牌。
> * （F）浏览器执行上一步获得的脚本，提取出令牌。
> * （G）浏览器将令牌发给客户端。
```
下面是上面这些步骤所需要的参数。

A步骤中，客户端发出的HTTP请求，包含以下参数：

response_type：表示授权类型，此处的值固定为"token"，必选项。
client_id：表示客户端的ID，必选项。
redirect_uri：表示重定向的URI，可选项。
scope：表示权限范围，可选项。
state：表示客户端的当前状态，可以指定任意值，认证服务器会原封不动地返回这个值。
下面是一个例子。


    GET /authorize?response_type=token&client_id=s6BhdRkqt3&state=xyz
        &redirect_uri=https%3A%2F%2Fclient%2Eexample%2Ecom%2Fcb HTTP/1.1
    Host: server.example.com

C步骤中，认证服务器回应客户端的URI，包含以下参数：

access_token：表示访问令牌，必选项。
token_type：表示令牌类型，该值大小写不敏感，必选项。
expires_in：表示过期时间，单位为秒。如果省略该参数，必须其他方式设置过期时间。
scope：表示权限范围，如果与客户端申请的范围一致，此项可省略。
state：如果客户端的请求中包含这个参数，认证服务器的回应也必须一模一样包含这个参数。
下面是一个例子。


     HTTP/1.1 302 Found
     Location: http://example.com/cb#access_token=2YotnFZFEjr1zCsicMWpAA
               &state=xyz&token_type=example&expires_in=3600

在上面的例子中，认证服务器用HTTP头信息的Location栏，指定浏览器重定向的网址。注意，在这个网址的Hash部分包含了令牌。

根据上面的D步骤，下一步浏览器会访问Location指定的网址，但是Hash部分不会发送。接下来的E步骤，服务提供商的资源服务器发送过来的代码，会提取出Hash中的令牌。
```
> #### 4.3 密码模式
> 密码模式（Resource Owner Password Credentials Grant）中，用户向客户端提供自己的用户名和密码。客户端使用这些信息，向"服务商提供商"索要授权。
> 在这种模式中，用户必须把自己的密码给客户端，但是客户端不得储存密码。这通常用在用户对客户端高度信任的情况下，比如客户端是操作系统的一部分，或者由一个著名公司出品。而认证服务器只有在其他授权模式无法执行的情况下，才能考虑使用这种模式。

![OAuth 2.0的运行流程](/resource/auth4.png)

>它的步骤如下：
> * （A）用户向客户端提供用户名和密码。
> * （B）客户端将用户名和密码发给认证服务器，向后者请求令牌。
> * （C）认证服务器确认无误后，向客户端提供访问令牌。
```
B步骤中，客户端发出的HTTP请求，包含以下参数：

grant_type：表示授权类型，此处的值固定为"password"，必选项。
username：表示用户名，必选项。
password：表示用户的密码，必选项。
scope：表示权限范围，可选项。
下面是一个例子。


     POST /token HTTP/1.1
     Host: server.example.com
     Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
     Content-Type: application/x-www-form-urlencoded

     grant_type=password&username=johndoe&password=A3ddj3w

C步骤中，认证服务器向客户端发送访问令牌，下面是一个例子。


     HTTP/1.1 200 OK
     Content-Type: application/json;charset=UTF-8
     Cache-Control: no-store
     Pragma: no-cache

     {
       "access_token":"2YotnFZFEjr1zCsicMWpAA",
       "token_type":"example",
       "expires_in":3600,
       "refresh_token":"tGzv3JOkF0XG5Qx2TlKWIA",
       "example_parameter":"example_value"
     }

上面代码中，各个参数的含义参见《授权码模式》一节。

整个过程中，客户端不得保存用户的密码。
```
> #### 4.4、客户端模式
> 客户端模式（Client Credentials Grant）指客户端以自己的名义，而不是以用户的名义，向"服务提供商"进行认证。严格地说，客户端模式并不属于OAuth框架所要解决的问题。在这种模式中，用户直接向客户端注册，客户端以自己的名义要求"服务提供商"提供服务，其实不存在授权问题。
>客户端模式
>它的步骤如下：
> * （A）客户端向认证服务器进行身份认证，并要求一个访问令牌。
> * （B）认证服务器确认无误后，向客户端提供访问令牌。

```
 A步骤中，客户端发出的HTTP请求，包含以下参数：

granttype：表示授权类型，此处的值固定为"clientcredentials"，必选项。
scope：表示权限范围，可选项。

     POST /token HTTP/1.1
     Host: server.example.com
     Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
     Content-Type: application/x-www-form-urlencoded

     grant_type=client_credentials

认证服务器必须以某种方式，验证客户端身份。

B步骤中，认证服务器向客户端发送访问令牌，下面是一个例子。


     HTTP/1.1 200 OK
     Content-Type: application/json;charset=UTF-8
     Cache-Control: no-store
     Pragma: no-cache

     {
       "access_token":"2YotnFZFEjr1zCsicMWpAA",
       "token_type":"example",
       "expires_in":3600,
       "example_parameter":"example_value"
     }

上面代码中，各个参数的含义参见《授权码模式》一节。
```
> ### 5、更新令牌
> 如果用户访问的时候，客户端的"访问令牌"已经过期，则需要使用"更新令牌"申请一个新的访问令牌。
> 客户端发出更新令牌的HTTP请求，包含以下参数：
```
granttype：表示使用的授权模式，此处的值固定为"refreshtoken"，必选项。
refresh_token：表示早前收到的更新令牌，必选项。
scope：表示申请的授权范围，不可以超出上一次申请的范围，如果省略该参数，则表示与上一次一致。
下面是一个例子。


     POST /token HTTP/1.1
     Host: server.example.com
     Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
     Content-Type: application/x-www-form-urlencoded

     grant_type=refresh_token&refresh_token=tGzv3JOkF0XG5Qx2TlKWIA

```


## 二、实现过程
### 1、新增权限认证服务器cloud-auth
* 在pom.xml中添加以下依赖
```
<!--引入 权限相关的包 -->
<dependency>
    <groupId>org.apache.shiro</groupId>
    <artifactId>shiro-core</artifactId>
    <version>1.3.2</version>
</dependency>
<dependency>
    <groupId>io.jsonwebtoken</groupId>
    <artifactId>jjwt</artifactId>
    <version>0.7.0</version>
</dependency>
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-security</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-oauth2</artifactId>
</dependency>

```
* 添加：[认证服务配置 AuthorizationServerConfig](/cloud-auth/src/main/java/com/hans/cloud/oauth/config/AuthorizationServerConfig.java)
* 添加：[安全服务配置 WebSecurityConfig](/cloud-auth/src/main/java/com/hans/cloud/oauth/config/WebSecurityConfig.java)
* 添加：[查询用户信息 UserDetailsServiceImpl](/cloud-auth/src/main/java/com/hans/cloud/oauth/service/impl/UserDetailsServiceImpl.java)
* 添加：[application.yml](/cloud-auth/src/main/resources/application.yml)
```
spring:
  application:
    name: oauth-server
  datasource:
    url: jdbc:mysql://47.96.140.221:3306/bigdataDB?useUnicode=true&characterEncoding=utf8&useSSL=false
    username: bigdata
    password: Bigdata!2019
    jackson:
      date-format: yyyy-MM-dd
      time-zone: GMT+8
    servlet:
      multipart:
        max-file-size: 100Mb
        max-request-size: 100Mb
mybatis:
  mapper-locations: mapper/**Mapper.xml
  configuration:
    map-underscore-to-camel-case: true

server:
  port: 8078

eureka:
  client:
    registerWithEureka: true
    fetchRegistry: true
    serviceUrl:
      defaultZone: http://localhost:8070/eureka/
  instance: 
    instanceId: ${spring.cloud.client.ip-address}:${spring.application.name}:${server.port}
#feign 对hytrix的支持
feign:
  hystrix:
    enabled: true

ribbon:
  ReadTimeout: 10000
  ConnectTimeout: 10000
  eureka:
    enabled: true
hystrix:
  command:
    default:
      execution:
        isolation:
          thread:
            timeoutInMilliseconds: 12000

```
### 2、修改网关程序，以支持权限拦截跳转到'认证服务器'
* 在pom.xml中添加依赖
```
<dependency>
    <groupId>org.springframework.security.oauth</groupId>
    <artifactId>spring-security-oauth2</artifactId>
    <version>2.2.1.RELEASE</version>
    <scope>compile</scope>
</dependency>
```
* 在网关中添加：[资源服务配置 ResourceServerConfig](/zuul-service/src/main/java/com/hans/cloud/zuulservice/config/ResourceServerConfig.java)
* 在网关中添加：[网关过滤器 AuthFilter](/zuul-service/src/main/java/com/hans/cloud/zuulservice/filter/AuthFilter.java)
* 在yml中添加：[application.yml](/zuul-service/src/main/resources/application.yml)
```
security:
  oauth2:
    client:
      #在认证服务器中配置的客户端账号
      client-id: client
      client-secret: client-secret
      access-token-uri: http://localhost:8078/oauth/token
      user-authorization-uri: http://localhost:8078/oauth/authorize
    resource:
      user-info-uri: http://localhost:8078/user
      token-info-uri: http://localhost:8078/oauth/check_token
      prefer-token-info: true
      jwt:
        key-uri: http://localhost:8078/oauth/token_key

```
### 3、资源服务器添加拦截
> 所谓的资源服务器，即对外提供服务的应用都可称之为资源服务器，通常也是eureak的client端。
* 在hello-server中添加资源拦截器配置
* 在hello-client中添加资源拦截配置



## 三、使用方法
* 此处省略300字...

## 四、常见问题（注意事项）
### 1、Spring Security Oauth2.0获取token报错如下：
```
{
    "error": "invalid_client",
    "error_description": "Bad client credentials"
}
```
* 解决办法：将ClientDetailsServiceConfigurer的configure里的.secret("secret")改为.secret(passwordEncoder().encode("secret"))即可
### 2、yml格式/语法问题
* 参数层级，变量名 冒号 空格 变量值
* 注释是# ，而不是//，否则在springBoot启动时报错
### 3、Spring Security – There is no PasswordEncoder mapped for the id “null”
* 因为 5.x 版本新增了多种密码加密方式，必须指定一种
```
@Bean
public PasswordEncoder passwordEncoder(){
	return new BCryptPasswordEncoder();
}
```
### 4、 调用接口/oauth/check_token 失败
返回错误结果如下：
```
{
  "timestamp": "2020-03-06T02:57:43.818+0000",
  "status": 403,
  "error": "Forbidden",
  "message": "Forbidden",
  "path": "/com-oauth/oauth/check_token"
}
```
解决方法：
在认证服务器中重写 configure(AuthorizationServerSecurityConfigurer security) 方法
```
security.tokenKeyAccess("permitAll()")
     .checkTokenAccess("isAuthenticated()");
```
### 5、在使用密码模式时，抛出异常：o.s.s.o.provider.endpoint.TokenEndpoint : Handling error: UnsupportedGrantTypeException, Unsupported grant type: password
解决方法：
在认证服务器中重写 configure(AuthorizationServerEndpointsConfigurer endpoints)
```
@Configuration
@EnableAuthorizationServer
public class OAuth2AuthorizationServer extends AuthorizationServerConfigurerAdapter {
   // 用户认证
   @Autowired
   private AuthenticationManager authenticationManager;
   @Override
   public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
       super.configure(endpoints);
       // 密码模式必须有这个参数
       endpoints.authenticationManager(authenticationManager);
   }
}
```

### 6、在 passwod 模式下，执行刷新 token 时，抛出异常 Handling error: IllegalStateException, UserDetailsService is required.
执行以下命令，抛出异常 Handling error: IllegalStateException, UserDetailsService is required

 curl -i -X POST -u 'clientapp2:112233'  http://.../oauth/token -H "accept: application/json" -d 'grant_type=refresh_token&refresh_token=b610dfa9-2ee4-4214-bc57-f6b2937d4b27'
解决方法：
在认证服务器中配置 UserDetailsService 对象
```
@Autowired
private UserDetailServiceImpl userDetailService;
@Override
public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
    //如果需要使用refresh_token模式则需要注入userDetailService
    endpoints.userDetailsService(userDetailService);
    // 密码模式必须有这个参数
    endpoints.authenticationManager(this.authenticationManager);
}
```
### 7、不支持 form 表单提交
执行命令：

curl -X POST "http://.../oauth/token" -d "grant_type=client_credentials&scope=read_contacts&client_id=clientapp&client_secret=112233"
返回错误：
```
{
    "timestamp":"2012-02-05T02:27:29.962+0000",
    "status":401,
    "error":"Unauthorized",
    "message":"Unauthorized",
    "path":"/oauth/token"
} 
```

解决方法：
在认证服务器中重写 configure(AuthorizationServerSecurityConfigurer security) 方法，让其支持表单提交 security.allowFormAuthenticationForClients();
```
@Override
public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
	security.allowFormAuthenticationForClients()
			.tokenKeyAccess("permitAll()")
			.checkTokenAccess("isAuthenticated()");
}
```
### 8、@EnableOAuth2Sso 引发的一个血的教训
```
* 在项目启动类 加上注解 @EnableOAuth2Sso
这一步就是错误的开始。。。
当你打开项目时， 会遇到各种 spring securiy 的问题， csrf、x-frame-options
当你只引入 spring security 时 这个问题很好解决

http.headers().frameOptions().disable().and().csrf().disable(); 
这样就可以解决, 但是由于集成了 @EnableOAuth2Sso ， 有一个默认的security 配置类
OAuth2SsoDefaultConfiguration 所以当你用 WebSecurityConfig 继承 WebSecurityConfigurerAdapter
去写 security 的配置类时 ， 项目起不起来， 报错原因 @Order 只能有一个 ，这时， 想到的最简单的办法就是 改变 @Order ，但是项目起来之后 发现 配置不生效。。。（目前还不知道什么原因）

* 其实最后的解决办法很简单，就是把 @EnableOAuth2Sso 放到自己写的配置类上

@EnableOAuth2Sso
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.headers().frameOptions().disable().and().csrf().disable();
        super.configure(http);
    }
}
```

