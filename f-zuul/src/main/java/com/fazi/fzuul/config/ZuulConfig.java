package com.fazi.fzuul.config;

import com.fazi.fzuul.filter.AccessFilter;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.cloud.netflix.zuul.filters.discovery.PatternServiceRouteMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @ClassName ZuulConfig
 * @Description 认证请求的配置 Bean
 * 现在的程序可以直接利用 zuul 的代理访问所有加密的微服务
 * @Author fazi
 * @Date 2020/3/1 23:46
 * @Version 1.0
 **/
@Configuration
public class ZuulConfig {

    @Bean
    public AccessFilter getAccessFilter(){
        return new AccessFilter();
    }

    // 借助PatternServiceRouteMapper实现路由的正则匹配
    // 将rest-demo-v1映射为/v1/rest-demo/**
//    @Bean
//    public PatternServiceRouteMapper serviceRouteMapper(){
//        /**
//         * A RegExp Pattern that extract needed information from a service ID. Ex :
//         * "(?<name>.*)-(?<version>v.*$)"
//         */
//        //private Pattern servicePattern;
//        /**
//         * A RegExp that refer to named groups define in servicePattern. Ex :
//         * "${version}/${name}"
//         */
//        //private String routePattern;
//        return new PatternServiceRouteMapper("(?<name>^.+)-(?<version>v.+$)", "${version}/${name}");
//    }

    @Bean
    @LoadBalanced// Spring Cloud Ribbon 是基于 Netflix Ribbon 实现的一套客户端
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }
}
