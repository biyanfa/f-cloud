package com.fazi.fzuul.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Set;

/**
 * @ClassName AccessFilter
 * @Description 进行授权访问处理
 * @Author fazi
 * @Date 2020/3/1 22:54
 * @Version 1.0
 **/
@Slf4j
public class AccessFilter extends ZuulFilter {

    // 在进行Zuul过滤的时候可以设置其过滤执行的位置，那么此时有如下几种类型：
    // 1、pre：在请求发出之前执行过滤，如果要进行访问，肯定在请求前设置头信息
    // 2、route：在进行路由请求的时候被调用；
    // 3、post：在路由之后发送请求信息的时候被调用；
    // 4、error：出现错误之后进行调用
    // 支持自定义 type
    @Override
    public String filterType() {
        return "pre";
    }

    // 表示相同 filterType 的顺序，设置优先级，数字越大优先级越低
    @Override
    public int filterOrder() {
        return 0;
    }

    // 此过滤器是否要执行
    @Override
    public boolean shouldFilter() {
        return true;
    }

    // 表示具体的过滤执行操作
    @Override
    public Object run() throws ZuulException {
        RequestContext currentContext = RequestContext.getCurrentContext();
        // 认证的原始信息
//        String auth = "fazi";
//        // 进行加密处理
//        byte[] encoded = Base64.getEncoder().encode(auth.getBytes(Charset.forName("US-ASCII")));
//        // 在进行授权的头信息内容配置的时候加密的信息一定要与“Basic”之间有一个空格
//        String authHeader = "Basic " + new String(encoded);
//        authHeader = "{noop}fazi";
//        currentContext.addZuulRequestHeader("authorization", authHeader);
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();

        log.info("send {} request to {}",request.getMethod(),request.getRequestURL().toString());
        System.out.println("token:" + request.getParameter("access_token"));
//        Object accessToken = request.getHeader("Authorization");
//        if (accessToken==null){
//            log.warn("Authorization token is empty");
//            requestContext.setSendZuulResponse(false);
//            requestContext.setResponseStatusCode(401);
//            requestContext.setResponseBody("Authorization token is empty");
//            return null;
//        }
        log.info("Authorization token is ok");
        return null;
    }
}
