package com.fazi.fresource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FResourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(FResourceApplication.class, args);
    }

}
