OpenFeign与Ribbon+restTemplate对比
---
## 1、OpenFeign与Ribbon+restTemplate区别
> feign和ribbon是Spring Cloud的Netflix中提供的两个实现软负载均衡的组件，Ribbon和Feign都是用于调用其他服务的，方式不同。Feign则是在Ribbon的基础上进行了一次改进，采用接口的方式。
* 1.启动类使用的注解不同，Ribbon 用的是@RibbonClient，Feign 用的是@EnableFeignClients。

* 2.服务的指定位置不同，Ribbon 是在@RibbonClient 注解上声明，Feign 则是在定义抽象方法的接口中使用@FeignClient 声明。

* 3.调用方式不同，Ribbon 需要自己构建 http 请求，模拟 http 请求然后使用 RestTemplate 发送给其他服务，步骤相当繁琐。

#### Feign

Feign 是在 Ribbon 的基础上进行了一次改进，是一个使用起来更加方便的 HTTP 客户端。采用接口的方式， 只需要创建一个接口，然后在上面添加注解即可 ，将需要调用的其他服务的方法定义成抽象方法即可， 不需要自己构建 http 请求。然后就像是调用自身工程的方法调用，而感觉不到是调用远程方法，使得编写 客户端变得非常容易。

#### Ribbon

Ribbon 是一个基于 HTTP 和 TCP 客户端 的负载均衡的工具。它可以 在客户端 配置 RibbonServerList（服务端列表），使用 HttpClient 或 RestTemplate 模拟 http 请求，步骤相当繁琐。

## 2、Ribbon+restTemplate示例说明
请参考[实验一：Zuul+Eureka+Ribbon集成.md](实验一：Zuul+Eureka+Ribbon集成.md)，在hello-server,hello-client中的代码实现。
> 创建RestTemplate实例的时候，使用@LoadBalanced注解可以将RestTemplate自动配置为使用负载均衡的状态。@LoadBanced将使用Ribbon为RestTemplate执行负载均衡策略。
> 创建负载均衡的RestTemplate不再能通过自动配置为创建，必须通过配置类创建，具体实例代码如下：
```
@Bean
@LoadBalanced //基于 Netflix Ribbon 实现的一种客户端负载均衡
public RestTemplate restTemplate() {
    return new RestTemplate();
}
```
## 3、OpenFeign示例说明
* 在[公共api组件,hello-api](/hello-api/src/main/java/com/hans/cloud/api/service/UserServiceAPI.java)中定义对外暴露的UserServiceAPI接口 ，以便在服务提供方和消费方中使用。
```
/**
 * 用户服务接口类
 */
public interface UserServiceAPI {
    /**
     * 根据用户id获取用户
     * @param id
     * @return
     */
    @GetMapping("/get")
    R get(@RequestParam(value = "id", required = false, defaultValue = "1") Long id );
}
```
* 在[服务提供方,hello-server](/hello-server/src/main/java/com/hans/helloserver/openfeign/UserOpenfeign.java)中实现上述的UserServiceAPI，并提供rest方式服务。
```
/**
 * userServie的具体实现
 */
@RestController
public class UserOpenfeign implements UserServiceAPI {

    @Value("${server.port}")
    private int port;

    @Override
    public R get(Long id) {
        UserDO user=new UserDO();
        user.setUserId(id);
        user.setUsername("port:"+port);
        user.setName("我是openfeign呀");
        return R.ok().put("data",user);
    }
}
```
* 在[服务消费方，hello-client](/hello-client/src/main/java/com/hans/helloclient/openfeign)中调用服务，并提供rest服务，以便于在postMan中测试。<br>
UserServiceAPClient.java
```
//@FeignClient（value = "HELLO-SERVER"） ，HELLO-SERVER是在eureka中注册的服务提供方名称（也就是你api接口实现类的工程名）
@FeignClient(value = "HELLO-SERVER", fallback = UserServiceAPIFallback.class,configuration = FeignConfiguration.class)
public interface UserServiceAPClient extends UserServiceAPI {

}
```
UserClient.java
```
@RestController
@RequestMapping("/feign/user")
public class UserClient {

    @Autowired
    private UserServiceAPClient userServiceAPI;

    /**
     * 根据用户id获取用户
     * @param id
     * @return
     */
    @GetMapping("/get")
    R get(@RequestParam(value = "id", required = false, defaultValue = "1") Long id ){
        return userServiceAPI.get(id);
    }
}
```
 [异常回调配置](/hello-client/src/main/java/com/hans/helloclient/openfeign/fallback)，当服务调用失效时执行的逻辑，并且需要注入到FeignClien中<br>
在application.yml中增加以下配置
```
#feign 对hytrix的支持
feign:
  hystrix:
    enabled: true
```

* 在[hello-client，入口程序](/hello-client/src/main/java/com/hans/helloclient/HelloClientApplication.java)中添加@EnableFeignClients(basePackages = {"com.hans.helloclient"})，保证能扫描@FeignClient注解
* 在postman中输入：http://localhost:8079/api/client/feign/user/get?id=32，可正常接收到结果。
